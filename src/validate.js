import { Table } from 'tableschema'

export async function validate (filePath, schema) {
  const table = await Table.load(filePath, {schema})
  // console.log(table)

  try {
    const rows = await table.read({ keyed: true })
    console.log(`${rows.length} rows were read`)
  } catch (error) {
    if (error.multiple) {
      for (const subError of error.errors) {
        console.error(`${filePath}:${subError.rowNumber}:${subError.columnNumber}: ${subError.message}`)
      }
    } else {
      console.error(error.message)
    }
  }
}
