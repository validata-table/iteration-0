# Qualidata - Iteration 0

[Wiki](https://git.opendatafrance.net/qualidata/qualidata-ui/wikis/home)

## Install

Required software:

- [NodeJS](https://nodejs.org/en/) LTS version (v8.9.4 at the time this line is written)
- [npm](https://www.npmjs.com/)

You can install them from your OS packages but we recommend using [nvm](https://github.com/creationix/nvm) to install NodeJS and npm:

```sh
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash
nvm install --lts
nvm use --lts
```

Install dependencies:

```sh
npm install
```
