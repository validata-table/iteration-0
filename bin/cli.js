import yargs from 'yargs'

import {validate} from '../src/validate.js'

const argv = yargs
  .usage('$0 <file>', 'Validate a CSV file against a table schema', (yargs) => {
    yargs.positional('file', {
      type: 'string',
      describe: 'The CSV file to validate'
    })
  })
  .option('schema', {
    describe: 'The table schema file to use',
    demandOption: true,
    default: 'schemas/prenom-schema.json'
  })
  .help()
  .argv

validate(argv.file, argv.schema)
